﻿using Battlehub.RTCommon;
using Battlehub.RTSL;
using Battlehub.RTSL.Interface;
using System.Collections;
using System.IO;
using UnityEngine;
using UnityObject = UnityEngine.Object;

namespace Battlehub.RTEditor
{
    public class GltfImporter : FileImporter
    {
        public override string FileExt
        {
            get { return ".gltf"; }
        }

        public override string IconPath
        {
            get { return "Importers/Obj"; }
        }

        public override int Priority
        {
            get { return int.MinValue; }
        }

        public override IEnumerator Import(string filePath, string targetPath)
        {
            yield return UnityGLTF.GLTFLoader.ins.Load(filePath).AsIEnumerator();

            //UnityGLTF.Cache.MeshCacheData[] meshDatas = UnityGLTF.GLTFLoader.ins._assetCache.MeshCache;
            //UnityGLTF.Cache.TextureCacheData[] textureDatas = UnityGLTF.GLTFLoader.ins._assetCache.TextureCache;
            //UnityGLTF.Cache.MaterialCacheData[] materialDatas = UnityGLTF.GLTFLoader.ins._assetCache.MaterialCache;

            //Mesh mesh = null;
            //mesh.name = Path.GetFileName(filePath) + "Mesh";

            GameObject go = UnityGLTF.GLTFLoader.ins.LastLoadedScene;
            //go.GetComponent<MeshFilter>().sharedMesh = mesh;
            go.name = Path.GetFileNameWithoutExtension(filePath);
            go.SetActive(false);

            //go.GetComponentInChildren<SkinnedMeshRenderer>().gameObject.AddComponent<TestComponent>();
            IProject project = IOC.Resolve<IProject>();
            IRuntimeEditor editor = IOC.Resolve<IRuntimeEditor>();

            //byte[] preview = null;

            //IResourcePreviewUtility previewUtility = IOC.Resolve<IResourcePreviewUtility>();
            //foreach (UnityGLTF.Cache.TextureCacheData textureData in textureDatas)
            //{
            //    preview = previewUtility.CreatePreviewData(textureData.Texture);
            //    yield return project.Save(targetPath, textureData.Texture, preview);
            //}

            //foreach (UnityGLTF.Cache.MaterialCacheData materialData in materialDatas)
            //{
            //    preview = previewUtility.CreatePreviewData(materialData.UnityMaterial);
            //    yield return project.Save(targetPath, materialData.UnityMaterial, preview);
            //}

            ProjectItem folder = project.GetFolder(Path.GetDirectoryName(targetPath));
            yield return editor.CreatePrefab(folder, go.AddComponent<ExposeToEditor>(), true, assetItems =>
            {
                //Resource resource = new Resource();
                //resource.path = filePath;
                //IAssetDB assetDB = IOC.Resolve<IAssetDB>();
                //foreach (AssetItem assetItem in assetItems)
                //{
                //    AssetData assetData = new AssetData();
                //    assetData.assetItem = assetItem;
                //    assetData.assetItemPath = assetItem.RelativePath(true);
                //    //assetData.obj = assetDB.FromID<UnityObject>(assetItem.ItemID);
                //    //assetData.id = DataManager.ins.GetAssetObjectID(assetData.obj);
                //    resource.assets.Add(assetData);
                    
                //}
                //DataManager.ins.data.resources.Add(resource);
            });

            //Object.Destroy(mesh);
            //UnityGLTF.GLTFLoader.ins.Dispose();
            Object.Destroy(go);
        }
    }
}
