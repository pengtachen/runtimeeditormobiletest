﻿using System.Collections;

using UnityEngine;
using UnityEngine.SceneManagement;

using Battlehub.RTCommon;
using Battlehub.RTSL.Interface;

public class LoadScene : MonoBehaviour
{
    IProject m_project;

    public string projectName;
    public string scenePath;

    IEnumerator Start()
    {
        m_project = IOC.Resolve<IProject>();
        yield return m_project.OpenProject(projectName); // Application.persistentDataPath + "/" + projectName
        yield return Load();
    }

    IEnumerator Load()
    {
        ProjectAsyncOperation ao = m_project.Load<Scene>(scenePath); // Application.persistentDataPath + "/" + projectName + "/Assets/" + scenePath
        yield return ao;

        if (ao.Error.HasError)
        {
            Debug.LogError(ao.Error.ToString());
        }
    }
}